import struct

async def initial_connection(self, data) -> None:
    state = self._connection
    state.ssrc = data["ssrc"]
    state.voice_port = data["port"]
    state.endpoint_ip = data["ip"]

    packet = bytearray(74)
    struct.pack_into(">H", packet, 0, 1)  # 1 = Send
    struct.pack_into(">H", packet, 2, 70)  # 70 = Length
    struct.pack_into(">I", packet, 4, state.ssrc)
    state.socket.sendto(packet, (state.endpoint_ip, state.voice_port))
    recv = await self.loop.sock_recv(state.socket, 70)

    # the ip is ascii starting at the 8th byte and ending at the first null
    ip_start = 8
    ip_end = recv.index(0, ip_start)
    state.ip = recv[ip_start:ip_end].decode("ascii")

    state.port = struct.unpack_from(">H", recv, 6)[0]

    # there *should* always be at least one supported mode (xsalsa20_poly1305)
    modes = [mode for mode in data["modes"] if mode in self._connection.supported_modes]

    mode = modes[0]
    await self.select_protocol(state.ip, state.port, mode)

def apply_fix():
    __import__("discord.gateway").gateway.DiscordVoiceWebSocket.initial_connection = initial_connection
    print("discord.gateway initial_connection FIXED")