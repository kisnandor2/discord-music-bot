from discord.ext import commands, tasks
import subprocess
from random import shuffle
from discord import VoiceClient, FFmpegPCMAudio
import datetime

# Parts of the bot that are common
# Currently these are:
# - connecting to the voice channel
# - stopping the audio and leaving the voice channel
# - manageing the playlist: clear, list, shuffle
# - a main loop which checks if there is any source to start and starts to play it in the connected voice channel
class CommonBot(commands.Cog):
  voice_client:VoiceClient = None
  playlist = []

  def __init__(self, bot):
    self.log("__init__")
    self.bot = bot
    self.MainLoop.start()

  async def ConnectClient(self, context):
    self.log("ConnectClient")
    user = context.message.author
    voice_channel = None
    try:
      voice_channel = user.voice.channel
    except:
      self.log("Could not get self.voice_channel")
      await context.channel.send("Could not connect to voice channel")
      return    
    if self.voice_client is None:
      self.log("self.voice_client is None, connecting to voice_channel...")
      self.voice_client = await voice_channel.connect()
      self.log("connected to voice channel")

  async def Play(self, context, url: str, message = None):
    self.log("Play")
    if self.voice_client is None:
      await self.ConnectClient(context)
    if message is not None:
      await context.channel.send(message)
    # https://stackoverflow.com/questions/50924411/using-ffmpeg-with-python-input-buffer-exhausted-before-end-element-found
    # -dn refers to no data encoding
    # -sn refers to no subtitles encoding
    # -ignore_unknown refers to ignore the unknown streams(SCTE 35, 128 data)
    # +discardcorrupt
    # TODO(nkis): fix the ffmpeg issue

    # options = `-report`` Dump full command line and console output to a file named program-YYYYMMDD-HHMMSS.log in the current directory. This file can be useful for bug reports. It also implies -loglevel verbose.
    source = FFmpegPCMAudio(source=url, before_options="-err_detect ignore_err -dn -sn -fflags +discardcorrupt", options="-err_detect ignore_err -dn -sn -ignore_unknown -ignore_unknown")
    self.voice_client.play(source, after = None)

  @commands.command(
    name = "restart",
    description = "Restart the bot in case it is not working",
    pass_context = True,
    help = "Executes a script which restarts the bot"
    )
  async def restart(self, context):
    self.log("restart")
    text_channel = context.channel
    await text_channel.send("***Restarting the bot...***")
    subprocess.call("systemctl --user restart lemontree_youtube_bot.service", shell=True)
    await text_channel.send("***Bot restart command started...***")

  @commands.command(
    name = "stop",
    description = "Stop the currently playing audio",
    pass_context = True,
    help = "Disconnect the bot from the audio channel. Also removes everything from the playlist."
    )
  async def stop(self, context):
    self.log("stop")
    self.voice_client = None
    self.playlist = []

    text_channel = context.channel
    await text_channel.send("**Stopping** currently playing audio and **leaving**")
    await context.voice_client.disconnect()

  @commands.command(
    name = "next",
    description = "Next song in the currently playing playlist",
    pass_context = True,
    help = "Skip the currently playing song to the next"
    )
  async def next(self, context):
    self.log("next")
    self.voice_client.stop()
    text_channel = context.channel
    await text_channel.send("**Skippig** currently playing song")

  @commands.command(
    name = "clear",
    description = "Clears the playlist",
    pass_context = True,
    help = "Clear the queued playlist"
    )
  async def clear(self, context):
    self.log("clear")
    await context.channel.send(f"**Removed** {len(self.playlist)} songs from the playlist")
    self.playlist = []

  @commands.command(
    name = "shuffle",
    description = "Shuffle current playlist",
    pass_context = True,
    help = "Randomize the playlist"
    )
  async def shuffle(self, context):
    self.log("shuffle")
    text_channel = context.channel
    await text_channel.send("**Shuffling** current playlist")
    shuffle(self.playlist)

  @commands.command(
    name = "list",
    description = "List all youtube urls that are in the current playlist",
    pass_context = True,
    help = "List all youtube urls that are in the current playlist"
    )
  async def list(self, context):
    self.log("list")
    if len(self.playlist) == 0:
      await context.channel.send("The playlist is currently empty")
      return
    msg = ""
    for i in range(len(self.playlist)):
      url = self.playlist[i]["url"]
      msg += f"Playlist[{i}]: {url}\n"
    await context.channel.send(msg)

  def VoiceClientIsBusy(self):
    return self.voice_client is not None and self.voice_client.is_playing()

  async def NextTrack(self):
    # self.log("NextTrack")
    if self.VoiceClientIsBusy():
      return
    to_play_next = None
    try:
      to_play_next = self.playlist.pop(0)
    except:
      return
    await self.Play(
      to_play_next["context"],
      to_play_next["url"],
      to_play_next["msg"]
    )

  def AddNextTrack(self, context, url: str, msg: str):
    self.log("AddNextTrack")
    self.playlist.append({
      "context": context,
      "url": url,
      "msg": msg
    })

  @tasks.loop(seconds = 1.0)
  async def MainLoop(self):
    # self.log("MainLoop")
    await self.NextTrack()

  def log(self, str):
    print(f"[{datetime.datetime.now()}] CommonBot: {str}")
