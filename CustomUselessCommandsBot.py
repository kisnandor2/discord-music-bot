from discord.ext import commands

# A class which creates some redundant commands
# but with funny (mostly Hungarian) keywords
class CustomUselessCommandsBot(commands.Cog):
  def __init__(self, bot):
    self.log("__init__")
    self.bot = bot

  @commands.command(
    name="kuss",
    description="Stop the currently playing audio",
    pass_context=True,
    help="Disconnect the bot from the audio channel. Also removes everything from the playlist.",
  )
  async def kuss(self, context):
    self.log("kuss")
    await self.bot.get_cog("CommonBot").stop(context)

  @commands.command(
    name="kotrógyinnen",
    description="Stop the currently playing audio",
    pass_context=True,
    help="Disconnect the bot from the audio channel. Also removes everything from the playlist.",
  )
  async def kotrogyinnen(self, context):
    self.log("kotrogyinnen")
    await self.bot.get_cog("CommonBot").stop(context)

  def log(self, str):
    print(f"CustomUselessCommandsBot: {str}")


