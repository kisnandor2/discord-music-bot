from discord.ext import commands
import pytube as pt
import datetime
from typing import Optional
from CommonBot import CommonBot

# Parts that are only for youtube:
# - search for the song
# - extract audio from youtube
# - pass the audio to the CommonBot
class YoutubeBot(commands.Cog):
  voice_client = None
  playlist = []

  def __init__(self, bot):
    self.log("__init__")
    self.bot = bot

  async def PlaySongs(self, context):
    self.log("PlaySongs")
    number_of_tracks_to_add = len(self.playlist)
    for i in range(number_of_tracks_to_add):
      self.log(f"Adding track #{i} to Common playlist")
      audio = None
      try:
        audio = self.GetNextAudio()
        if audio is None:
          self.log("Audio is None")
          raise Exception("Audio is None")
      except Exception as e:
        await context.channel.send(f"Could not start the next song. Some error occured. Was it a livestream? We do not support it. See logs for more info")
        self.log(f"Exception raised: {e}")
        continue
      msg = f"Will try to play: {audio.title} in {context.message.author.voice.channel.name} channel"
      common_bot: CommonBot = self.bot.get_cog("CommonBot")
      common_bot.AddNextTrack(
        context= context,
        url= audio.url,
        msg= msg
      )

  def ParseArgs(self, args):
    self.log("ParseArgs")
    possible_url = args[0]
    if "http" in possible_url:
      if "playlist" in possible_url or "list" in possible_url:
        for url in pt.Playlist(possible_url).video_urls:
          self.playlist.append(url)
          if len(self.playlist) > 40:
            self.playlist = self.playlist[:40]
      else:
        self.playlist.append(possible_url)
    else:
      search_term = " ".join(args)
      possible_url = pt.Search(search_term).results[0].watch_url
      self.log(f"For the search term {search_term} I've found the url: {possible_url}")
      self.playlist.append(possible_url)

  def GetNextAudio(self) -> Optional[pt.Stream]:
    self.log("GetNextAudio")
    video_url = self.playlist.pop(0)
    video = pt.YouTube(video_url)
    audio = video.streams.get_audio_only()
    return audio

  @commands.command(
    name = "play",
    description = "Plays the youtube audio in the current voice channel",
    pass_context = True,
    help = "Start playing the specified youtube url or play the first search result for your input text. Also queues the item if something is currently playing",
    )
  async def play(self, context, *args):
    self.log("play")
    try:
      context.message.author.voice.channel.name
    except:
      await context.channel.send(f"Dear {context.message.author}, you are not connected to any voice channel. Can't start any playlist...")
      return
    old_length = len(self.playlist)
    self.ParseArgs(args)
    new_length = len(self.playlist)
    await context.channel.send(f"Added {new_length - old_length} song(s) to the playlist")
    await self.PlaySongs(context)

  def log(self, str):
    print(f"[{datetime.datetime.now()}] YoutubeBot: {str}")