from discord.ext import commands
from CommonBot import CommonBot
import datetime

class RadioBot(commands.Cog):
  radio_stations = {
    "danu": "https://www.danubiusradio.hu/live_320.mp3/",
    "danub": "https://www.danubiusradio.hu/live_320.mp3/",

    "juv": "https://s2.audiostream.hu/juventus_320k",
    "juventus": "https://s2.audiostream.hu/juventus_320k",

    "magic": "https://live.magicfm.ro:8443/magicfm.aacp",
    "paprika": "http://stream1.paprikaradio.ro:8000",
    "petofi": "https://icast.connectmedia.hu/4738/mr2.mp3",
    "radio1": "https://icast.connectmedia.hu/5202/live.mp3",
    "retro": "https://icast.connectmedia.hu/5002/live.mp3",
    "rock": "https://s2.audiostream.hu/bdpstrock_192k",
    "slager": "http://92.61.114.159:7812/slagerfm256.mp3",

		"roxy": "https://s2.audiostream.hu/roxy_320k",
		"roxi": "https://s2.audiostream.hu/roxy_320k",

    "xmas": "https://stream1.christmasfm.hu/med.mp3",
    "x": "https://stream1.christmasfm.hu/med.mp3",
  }  

  def __init__(self, bot):
    self.log("__init__")
    self.bot = bot
    self.last_played_radio_station = {}

  @commands.command(
    name = "rlist",
    description = "List the available radios",
    pass_context = True,
    help = "List the available radios"
  )
  async def rlist(self, context):
    self.log("rlist")
    msg = "Here are the radio stations that we currently support:\n\n"
    msg += "\n".join(f"{radio_station} - {url}" for radio_station, url in self.radio_stations.items())    
    await context.channel.send(msg)

  @commands.command(
    name = "rplay",
    description = "Start a radio station",
    pass_context = True,
    help = "Start a radio station. Use -rlist to get available radios"
  )
  async def rplay(self, context, *args):
    self.log("rplay")

    try:
      context.message.author.voice.channel.name
    except:
      await context.channel.send(f"Dear {context.message.author}, you are not connected to any voice channel. Can't start any radio...")
      return

    radio_station_selected = "".join(args)
    if radio_station_selected not in self.radio_stations.keys():
      await context.channel.send(f"{radio_station_selected} could not be found. Did you mistype it? Use -rlist to check the radio stations")
      return

    await context.channel.send(f"{radio_station_selected} added to playlist. Use -next if needed")
    radio_station_url = self.radio_stations[radio_station_selected]
    self.last_played_radio_station["url"] = radio_station_url
    self.last_played_radio_station["station"] = radio_station_selected
    common_bot: CommonBot = self.bot.get_cog("CommonBot")
    common_bot.AddNextTrack(
      context= context,
      url= radio_station_url,
      msg= f"Will try to **play** a radio: **{radio_station_selected}**"
    )

  @commands.command(
    name = "r",
    description = "Restart the currently playing radio station",
    pass_context = True,
    help = "Restart the radio station. Useful when something bugs and you want to restart it"
  )
  async def r(self, context, *args):
    try:
      _ = self.last_played_radio_station["url"]
    except:
      return    
    common_bot: CommonBot = self.bot.get_cog("CommonBot")    
    common_bot.AddNextTrack(
      context= context,
      url= self.last_played_radio_station["url"],
      msg= f"Will try to **restart** a radio: **{self.last_played_radio_station['station']}**"
    )
    await common_bot.next(context)

  @commands.command(
    name = "rr",
    description = "Start radio Roxy",
    pass_context = True,
    help = "Start radio roxy"
  )
  async def rr(self, context, *args):
    self.log("rr")
    radio_station_url = self.radio_stations["roxy"]
    radio_station_selected = "roxy"
    self.last_played_radio_station["url"] = radio_station_url
    self.last_played_radio_station["station"] = radio_station_selected
    common_bot: CommonBot = self.bot.get_cog("CommonBot")
    common_bot.AddNextTrack(
      context= context,
      url= radio_station_url,
      msg= f"Will try to **play** a radio: **{radio_station_selected}**"
    )

  def log(self, str):
    print(f"[{datetime.datetime.now()}] RadioBot: {str}")
