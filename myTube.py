import datetime
from discord import Intents
from discord.ext import commands
import logging

from YoutubeBot import YoutubeBot
from CommonBot import CommonBot
from RadioBot import RadioBot
from CustomUselessCommandsBot import CustomUselessCommandsBot

from VoiceClientFix import apply_fix

intents = Intents.all()

bot = commands.Bot(command_prefix='-', intents=intents)

@bot.event
async def setup_hook():
  # Add Cogs to Bot
  log("Adding cogs")  
  await bot.add_cog(CommonBot(bot))
  await bot.add_cog(YoutubeBot(bot))
  await bot.add_cog(RadioBot(bot))
  await bot.add_cog(CustomUselessCommandsBot(bot))
  log("Cogs added")

@bot.event
async def on_ready():
  log("on_ready")
  log(datetime.datetime.now())
  # Tell the users from LemonTree server that the Bot is up and running
  try:
    log("Sending `ready` message to LemonTree server")
    zenemalacz_channel = bot.get_channel(784022447183757323)
    await zenemalacz_channel.send("LemonTreeBot up and running :white_check_mark:")
  except Exception as e:
    log(f"Could not send `ready` message to LemonTree server ${e}")


def main():
  # Apply Voice Client fix: https://github.com/Rapptz/discord.py/issues/9277
  apply_fix()
  # Bok Token
  token = "OTQyODM2NTUxMzU0ODQzMTg2.G5lTXk.f7WUGLUnfLCZqcfo2QgLkkkptHzYuTNFZA6R24"  
  # Logger
  # handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
  # Start the bot
  bot.add_command(connect)
  # bot.run(token, log_handler=handler, log_level=logging.DEBUG)
  bot.run(token)

@commands.command()
async def connect(context):
    log("connect")
    user = context.message.author
    voice_channel = None
    voice_client = None
    try:
      voice_channel = user.voice.channel
    except:
      log("Could not get voice_channel")
    if voice_channel != None:
      if voice_client is None:
        log("voice_client is None")
        log(type(voice_channel))
        voice_client = await voice_channel.connect()
        log(f"voice_client is {voice_channel}")

def log(str):
  print(f"myTube: {str}")

if __name__ == "__main__":
  main()
